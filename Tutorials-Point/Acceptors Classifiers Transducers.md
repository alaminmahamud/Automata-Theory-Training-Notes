# Acceptor - Recognizer:
------------------------

An automation that computes a boolean function is called an acceptor. All the states of an accpetor is either accepting or rejecting the inputs given to it.

# Classifier
------------

A classifier has more than two final states and it gives a single output when it terminates.

# Transducer
------------

An automation that produces outputs based on current input and/or previous state is called a transducer. Transducers can be of two types −

1. Mealy Machine − The output depends only on the current state.

2. Moore Machine − The output depends both on the current input as well as the current state.
