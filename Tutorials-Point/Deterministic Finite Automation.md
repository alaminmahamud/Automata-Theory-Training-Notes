# Finite Automation can be classified into two types - 

1. Deterministic FInite Automation DFA
2. NDFA / NFA

# DFA

In DFA, for each input symbol, one can determine the state to which the machine will move. Hence, it is called Deterministic Automaton. As it has a finite number of states, the machine is called Deterministic Finite Machine or Deterministic Finite Automaton.

# Graphical representation of a DFA

a Dfa is represented by digraphs called state diagram

- The vertices represent the states.
- The arcs labeled with an input alphabet show the transitions.
- The initial state is denoted by an empty single incoming arc.
- The final state is indicated by double circles.



