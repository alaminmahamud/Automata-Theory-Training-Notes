# Automata - What is it?

The term "Automata" is derived from the Greek word "αὐτόματα" wchich means "self-acting". An automation (automata in plural) is an abstract self-propelled computing device which follows a predetermined sequence of operations automatically.

# Finite State Machines [fsm]

An automaton with a finite number of states is called a Finite Automaton (FA) or Finite State Machine (FSM)

# Formal Definitions of Finite Automation

An Automation can be represented by a 5-tuple(Q, ∑, δ, q0, F) where -
in turns 
1. finite set 
2. finite set of symbols, called the alphabet of the automation
3. transition function
4. initial state from where any input is processed (q0 is element of Q)
5. F is a set of final state/states of Q (F is a subset of Q)


