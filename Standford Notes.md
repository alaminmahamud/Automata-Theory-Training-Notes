Through Automata computer scientists are able to understand how machines compute functions and solve problems and more importantly, what it means for a function to be defined as computable and for a question to be described as decidable

Automatons are abstract models of machines that perform computations on an input by moving through a series of states or configurations. At each state of the computation, a transition function determines the next configuration on the basis of a finite portion of the present configuration. As a result, once the computation reaches an accepting configuration, it accepts that input. The most general and powerful automata is the Turing machine

The major objective of automata theory is to develop methods by which computer scientists can describe and analyze the dynamic behavior of discrete systems, in which signals are sampled periodically. The behavior of these discrete systems is determined by the way that the system is constructed from storage and combinational elements.


# four major families of automation :

	1. Finite-state machine
	2. Pushdown automata
	3. Linear-bounded automata
	4. Turing machine
	
	
# Finite State Machines
	
Input Symbol -> Defining Function -> Output Symbol

Defining Function is considered to be a black box 

old state->new state

several types of finite-state machines, which can be divided into three main categories

- acceptors - either accept the input or do not
- recognizers - either recognize the input or do not
- transducers - generate output from given input



Applications of finite-state machines are found in a variety of subjects. They can operate on languages with a finite number of words (standard case) an infinite number of words (Rabin automata, Birche automata), various number of treesm and in hardware circuits, where the input, the state and the output are bit vectors of a fixed size.






-----------------------------------------
Applications of Automata Theory

Automata theory is the basis for the theory of formal language. A Proper treatment of formal language theory begins with some basic definitions -

1. 
